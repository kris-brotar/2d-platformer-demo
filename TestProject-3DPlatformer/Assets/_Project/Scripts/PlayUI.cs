﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Invector.CharacterController;


public class PlayUI : MonoBehaviour {

    #region Private Variables
    [SerializeField]
    private Image healthBar;
    [SerializeField]
    private vCharacter player;
    #endregion

    #region Monobehavior Methods
    // Update is called once per frame
    void Update()
    {
        healthBar.fillAmount = (float)player.currentHealth / player.maxHealth;
    }
    #endregion
}
