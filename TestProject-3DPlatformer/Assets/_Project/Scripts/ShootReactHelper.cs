﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.CharacterController;

namespace Brotar
{
    /// <summary>
    /// This class handles player reaction while shooting.
    /// </summary>
    public class ShootReactHelper : MonoBehaviour
    {
        #region Public Variables
        private bool isReacting;
        public bool IsReacting
        {
            get { return isReacting; }
        }

        #endregion

        #region Private Variables
        // Current direction player is moving now. This should have certain value even if player is not moving.
        // Basically, this stores last movement direcion.
        //private Vector3 _currentDir;
        private float _lastXInput;

        private vShooterManager _shooterManager;
        private vThirdPersonInput _inputManager;
        private vThirdPersonController _motor;

        private float reactTimer;
        #endregion

        #region Monobehavior Methods
        void Start()
        {
            _shooterManager = GetComponent<vShooterManager>();
            _inputManager = GetComponent<vThirdPersonInput>();
            _motor = GetComponent<vThirdPersonController>();
        }

        void Update()
        {
            if (_shooterManager.isShooting)
            {
                
            }
            /*
            if (_motor.speed > Mathf.Epsilon)
            {
                //_currentDir = _motor.targetDirection;
                _lastXInput = _inputManager.horizontalInput.GetAxis();
            }
            //Debug.DrawRay(transform.position + new Vector3(0f, 0.5f, 0f), _currentDir.normalized, Color.red);

            // If there is no controller input, we make character move backward while shooting.
            if (_shooterManager.isShooting && Mathf.Abs(_inputManager.horizontalInput.GetAxis()) < Mathf.Epsilon)
            {
                //_motor.input.x = - Mathf.Sign(_lastXInput) * 0.5f;
                //_motor.speed = 1.0f;
                //_motor.input.x = -0.5f;
            }
            */
        }
        #endregion

        #region Public Methods

        #endregion

        #region Private Methods

        #endregion
    }
}