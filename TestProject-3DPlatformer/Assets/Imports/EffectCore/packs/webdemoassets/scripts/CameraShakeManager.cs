﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Brotar.Library;

public class CameraShakeManager : Singleton<CameraShakeManager> {

    //public bool cameraShakeBool = true;
    public Animator CamerShakeAnimator;
    
    public void ShakeCamera()
    {
        CamerShakeAnimator.SetTrigger("CameraShakeTrigger");
    }
}
